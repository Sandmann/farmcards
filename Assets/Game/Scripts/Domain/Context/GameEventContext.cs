namespace Assets.Game.Domain.Contexts
{
    using System;
    using Core.Common;
    using Models;

    [Serializable]
    public class GameEventContext : Context<GameEventModel>
    {
        public int Turn;
        public int Value;
    }
}
