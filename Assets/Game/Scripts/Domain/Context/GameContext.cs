using System.Collections.Generic;

namespace Assets.Game.Domain.Contexts
{
    using System;
    using Core.Common;
    using Models;

    [Serializable]
    public class GameContext : Context<GameModel>
    {
        public int Gold;

        public int CurEnergy;
        public int MaxEnergy;
    }
}
