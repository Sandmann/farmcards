namespace Assets.Game.Domain.Contexts
{
    using System;
    using Core.Common;
    using Models;

    [Serializable]
    public class CardContext : Context<CardModel>
    {
        public CardZone Zone = CardZone.None;
    }
}
