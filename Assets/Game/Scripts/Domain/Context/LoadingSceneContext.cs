using System;
using Assets.Game.Core.Common;
using Assets.Game.Domain.Models;

namespace Assets.Game.Domain.Contexts
{
    [Serializable]
    public class LoadingSceneContext : Context<LoadingSceneModel>
    {
        public bool IsLoadingDone;
        public float LoadingProgress;

        public float SceneLoadingAlpha;
    }
}