using UnityEngine.Serialization;

namespace Assets.Game.Domain.Contexts
{
    using System;
    using Core.Common;
    using Models;

    [Serializable]
    public class CropsContext : Context<CropsModel>
    {
        public int CurWater;
        public int CurLifeTime;
        public int GoldReward;

        public bool IsDead => CurWater == 0;
        public bool IsRipe => CurLifeTime == 0;
        public bool CanCollect => IsDead || IsRipe;
    }
}
