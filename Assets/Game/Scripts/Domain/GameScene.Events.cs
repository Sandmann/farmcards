﻿using System.Threading.Tasks;
using Assets.Game.Domain.Models;
using Game.Scripts.Domain.System;
using Game.Scripts.Domain.View;
using UnityEngine;
using UnityEngine.AddressableAssets;

namespace Assets.Game.Domain
{
    using Contexts;
    using Assets.Game.Core.Common;

    public partial class GameScene
    {
        public void GameOver()
        {
            _gameOverScreen.SetActive(true);
        }

        public async void RestartGame()
        {
            GameState.Current = null;
            await LoadingScene.Instance.LoadGameScene();
        }

        public void ExitGame()
        {
#if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
#else
            Application.Quit();
#endif
        }

        public bool TryApplyCardToSlot(FarmSlotView slot, CardContext card)
        {
            var gameCtx = GameState.Current.GameContext;
            if (gameCtx.CurEnergy < card.Model.EnergyCost)
            {
                return false;
            }
            
            var resolved = ContextSystem.GetOrCreate<CardEffectSystem>().TryResolveEffectInSlot(card, slot);
            if (resolved)
            {
                gameCtx.CurEnergy -= card.Model.EnergyCost;
                gameCtx.UpdateViews();
            }

            return resolved;
        }

        public void CollectCropsFromSlot(FarmSlotView slot)
        {
            var cropsCtx = slot.GetContext();
            if (cropsCtx.IsRipe)
            {
                var gameCtx = GameState.Current.GameContext;
                gameCtx.Gold += cropsCtx.Model.GoldReward;
                gameCtx.UpdateViews();
            }

            slot.Detach();
            GameState.Current.Crops.Remove(cropsCtx);
        }

        public async Task SelectCardToRemoveFromDeck()
        {
            var handle = Addressables.LoadAssetAsync<GameObject>("CardView");
            var prefab = await handle.Task;
            
            var cards = ContextSystem.GetOrCreate<DeckSystem>().GetRandomPlayerCards(3);
            for (int i = 0; i < cards.Length; i++)
            {
                var view = Instantiate(prefab, _changeDeckView.CardsRoot).GetComponent<CardView>();
                view.ViewMode = CardViewMode.Selector;
                view.Attach(cards[i]);
            }
            
            await _changeDeckView.ShowRemoveCardFromDeck();
            
            Addressables.Release(handle);
        }

        public void SelectCardForGameEvent(CardView cardView)
        {
            // if (ScreenState.Current == ScreenStates.SelectCardToAdd)
            // {
            //     cardView.transform.SetParent(_handRoot);
            //     cardView.ViewMode = CardViewMode.Gameplay;
            //     
            //     AddCardIntoDeck(cardView.GetContext());
            // }
            // else if (ScreenState.Current == ScreenStates.SelectCardToRemove)
            // {
            //     RemoveCardFromDeck(cardView.GetContext());
            // }
            
            _changeDeckView.Hide();
        }
        
        private void AddCardIntoDeck(CardContext cardCtx)
        {
            cardCtx.Zone = CardZone.Deck;
            cardCtx.UpdateViews();
            
            GameState.Current.Cards.Add(cardCtx);
        }

        private void RemoveCardFromDeck(CardContext cardCtx)
        {
            cardCtx.Zone = CardZone.Removed;
            cardCtx.UpdateViews();
            
            GameState.Current.Cards.Remove(cardCtx);
        }
        
        public async Task SelectCardToAddIntoDeck()
        {
            var handle = Addressables.LoadAssetAsync<GameObject>("CardView");
            var prefab = await handle.Task;
            
            var cards = ContextSystem.GetOrCreate<DeckSystem>().CreateRandomCards(3);
            for (int i = 0; i < cards.Length; i++)
            {
                var view = Instantiate(prefab, _changeDeckView.CardsRoot).GetComponent<CardView>();
                view.ViewMode = CardViewMode.Selector;
                view.Attach(cards[i]);
            }
            
            await _changeDeckView.ShowAddCardIntoDeck();
            
            Addressables.Release(handle);
        }
    }
}
