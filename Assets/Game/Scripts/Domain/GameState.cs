﻿namespace Assets.Game.Domain
{
    using System;
    using System.Collections.Generic;
    using Contexts;

    [Serializable]
    public class GameState
    {
        #region Game Constants

        public const string Version = "0.1";
        public const string GameSceneName = "GameScene";

        #endregion
        
        public static GameState Current { get; set; }

        public GameContext GameContext = null;
        public LocalizationContext LocalizationContext = null;

        #region Context collection
        
        public List<CardContext> Cards = new List<CardContext>();
        public List<CropsContext> Crops = new List<CropsContext>();
        public List<GameEventContext> Events = new List<GameEventContext>();

        #endregion
    }
}
