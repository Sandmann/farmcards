using System.Linq;
using System.Threading.Tasks;
using Assets.Extensions;
using Assets.Game.Core.Common;
using Assets.Game.Domain;
using Assets.Game.Domain.Contexts;
using Assets.Game.Domain.Models;
using Unity.VisualScripting;
using UnityEngine;

namespace Game.Scripts.Domain.System
{
    public class DeckSystem : ContextSystem
    {
        public override async Task SetupAsync()
        {
            var models = GameState.Current.GameContext.Model.StartingDeck;
            for (int i = 0; i < models.Length; i++)
            {
                var ctx = CreateContext<CardContext, CardModel>(models[i]);
                ctx.Zone = CardZone.Deck;
                
                GameState.Current.Cards.Add(ctx);
            }

            ShuffleDeck();
            await base.SetupAsync();
        }

        public CardContext[] CreateRandomCards(int count)
        {
            var cards = new CardContext[count];
            var models = GameState.Current.GameContext.Model.EventsCard.ToArray();
            models.Shuffle();
            models = models
                .Take(count)
                .ToArray();
            
            for (int i = 0; i < models.Length; i++)
            {
                cards[i] = CreateContext<CardContext, CardModel>(models[i]);
            }

            return cards;
        }

        private int HandSize => GameState.Current.GameContext.Model.HandSize;
        private CardContext[] CurDeck => GameState.Current.Cards
            .Where(ctx => ctx.Zone == CardZone.Deck)
            .ToArray();
        
        private CardContext[] CurHand=> GameState.Current.Cards
            .Where(ctx => ctx.Zone == CardZone.Hand)
            .ToArray();

        public CardContext[] GetRandomPlayerCards(int count)
        {
            var cards = GameState.Current.Cards
                .Where(ctx => ctx.Zone != CardZone.None)
                .DistinctBy(x => x.Model.Id)
                .ToArray();
            
            cards.Shuffle();
            
            return cards
                .Take(count)
                .ToArray();
        }

        public void DiscardHand()
        {
            var hand = CurHand;
            for (int i = 0; i < hand.Length; i++)
            {
                hand[i].Zone = CardZone.Discard;
                hand[i].UpdateViews();
            }
        }

        public void DrawHand()
        {
            if (CurDeck.Length < HandSize)
            {
                PutDiscardIntoDeck();
                ShuffleDeck();
            }
            
            var hand = CurDeck
                .Take(HandSize)
                .ToArray();
            
            for (int i = 0; i < hand.Length; i++)
            {
                hand[i].Zone = CardZone.Hand;
                hand[i].UpdateViews();
            }
        }

        private void PutDiscardIntoDeck()
        {
            var discrad = GameState.Current.Cards
                .Where(ctx => ctx.Zone == CardZone.Discard)
                .ToArray();

            for (int i = 0; i < discrad.Length; i++)
            {
                discrad[i].Zone = CardZone.Deck;
            }
        }

        private void ShuffleDeck()
        {
            GameState.Current.Cards.Shuffle();
        }
    }
}
