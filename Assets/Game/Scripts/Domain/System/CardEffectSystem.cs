using Assets.Game.Core.Common;
using Assets.Game.Domain;
using Assets.Game.Domain.Contexts;
using Assets.Game.Domain.Models;
using Game.Scripts.Domain.View;

namespace Game.Scripts.Domain.System
{
    public class CardEffectSystem : ContextSystem
    {
        public bool TryResolveEffectInSlot(CardContext card, FarmSlotView slot)
        {
            if (card.Model.CropsModel)
            {
                if (slot.GetContext() != null)
                {
                    return false;
                }
                
                var cropsCtx = GetOrCreate<CropsSystem>().CreateCropsByCard(card);
                slot.Attach(cropsCtx);
            }

            if (card.Model.Effect == CardEffect.AddWaterToSingleCrops)
            {
                if (slot.GetContext() == null || slot.GetContext().CanCollect)
                {
                    return false;
                }

                slot.GetContext().CurWater += card.Model.EffectValue;
                slot.GetContext().UpdateViews();
            }

            card.Zone = CardZone.Discard;
            card.UpdateViews();
            return true;
        }
    }
}