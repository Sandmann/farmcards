using System.Linq;
using System.Threading.Tasks;
using Assets.Extensions;
using Assets.Game.Core.Common;
using Assets.Game.Domain;
using Assets.Game.Domain.Contexts;
using Assets.Game.Domain.Models;
using UnityEngine;

namespace Game.Scripts.Domain.System
{
    public class GameEventSystem : ContextSystem
    {
        public override Task SetupAsync()
        {
            var models = GameState.Current.GameContext.Model.Events;
            for (int i = 0; i < models.Length; i++)
            {
                var ctx = CreateContext<GameEventContext, GameEventModel>(models[i]);
                ctx.Turn = 0;

                if (ctx.Model.Type == GameEvent.RentCollect)
                {
                    ctx.Value = GameState.Current.GameContext.Model.BasicRent;
                }
                
                GameState.Current.Events.Add(ctx);
            }
            
            return base.SetupAsync();
        }

        private GameEventContext[] CurEvents => GameState.Current.Events
            .Where(x => x.Turn > 0)
            .ToArray();

        public async Task UpdateEvents()
        {
            var events = CurEvents;
            for (int i = 0; i < events.Length; i++)
            {
                events[i].Turn--;

                if (events[i].Turn == 0)
                {
                    if (events[i].Model.Type == GameEvent.RentCollect)
                    {
                        CollectRent(events[i]);
                    }
                    if (events[i].Model.Type == GameEvent.AddNewCards)
                    {
                        await GameScene.Instance.SelectCardToAddIntoDeck();
                    }
                    if (events[i].Model.Type == GameEvent.RemoveCards)
                    {
                        await GameScene.Instance.SelectCardToRemoveFromDeck();
                    }
                }
                
                events[i].UpdateViews();
            }

            if (CurEvents.Length == 0)
            {
                // TODO: select next event
                var anyEvent = GameState.Current.Events.GetRandom();
                anyEvent.Turn = GameState.Current.GameContext.Model.EventSlotCount - 1;
                anyEvent.UpdateViews();
            }
        }

        private void CollectRent(GameEventContext eventCtx)
        {
            var gameCtx = GameState.Current.GameContext;
            var rent = eventCtx.Value;
            if (rent > gameCtx.Gold)
            {
                GameScene.Instance.GameOver();
                return;
            }

            gameCtx.Gold -= rent;
            gameCtx.UpdateViews();

            eventCtx.Value = Mathf.FloorToInt(1.3f * eventCtx.Value);
        }
    }
}
