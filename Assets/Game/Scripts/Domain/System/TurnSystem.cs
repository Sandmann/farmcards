using System.Threading.Tasks;
using Assets.Game.Core.Common;
using Assets.Game.Domain;
using UnityEngine;

namespace Game.Scripts.Domain.System
{
    public class TurnSystem : ContextSystem
    {
        public async Task NextTurn()
        {
            var gameCtx = GameState.Current.GameContext;
            gameCtx.CurEnergy = gameCtx.MaxEnergy;
            gameCtx.UpdateViews();
            
            //TODO: state machine has more control; refactor to ScreenState
            await GetOrCreate<GameEventSystem>().UpdateEvents();
            
            GetOrCreate<CropsSystem>().UpdateCrops();
            
            GetOrCreate<DeckSystem>().DiscardHand();
            GetOrCreate<DeckSystem>().DrawHand();
        }
    }
}
