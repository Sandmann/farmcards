using System.Linq;
using Assets.Game.Core.Common;
using Assets.Game.Domain;
using Assets.Game.Domain.Contexts;
using Assets.Game.Domain.Models;

namespace Game.Scripts.Domain.System
{
    public class CropsSystem : ContextSystem
    {
        public CropsContext CreateCropsByCard(CardContext cardCtx)
        {
            var cropsModel = cardCtx.Model.CropsModel;
            var cropsCtx = CreateContext<CropsContext, CropsModel>(cropsModel);
            cropsCtx.CurWater = cropsModel.WaterOnStart;
            cropsCtx.CurLifeTime = cropsModel.LifeTime;
            
            GameState.Current.Crops.Add(cropsCtx);
            return cropsCtx;
        }

        public void UpdateCrops()
        {
            var crops = GameState.Current.Crops
                .Where(x => !x.CanCollect)
                .ToArray();
            
            for (int i = 0; i < crops.Length; i++)
            {
                crops[i].CurLifeTime--;
                if (crops[i].IsRipe == false)
                {
                    crops[i].CurWater--;
                }
                
                crops[i].UpdateViews();
            }
        }
    }
}
