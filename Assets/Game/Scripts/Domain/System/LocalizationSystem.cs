﻿namespace Assets.Game.Domain.Systems
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text.RegularExpressions;
    using System.Threading.Tasks;
    using Assets.Game.Domain.Contexts;
    using Core.Common;
    using Models;
    using UnityEngine;

    [Serializable]
    public class LocalizationSystem : ContextSystem
    {
        private readonly string[] _tags = new[]
        {
            "TODO: Add new tags to parse localization strings",
        };

        private Dictionary<string, string> _keys = new Dictionary<string, string>();

        public override async Task SetupAsync()
        {
            if (GameState.Current.LocalizationContext == null)
            {
                GameState.Current.LocalizationContext = 
                    await CreateContextAsync<LocalizationContext, LocalizationModel>("LocalizationModel");
            }            

            // TODO: Replace with system language
            var ctx = GameState.Current.LocalizationContext;
            if (ctx.Model.Languages.Length > 0)
            {   
                LoadLanguage(ctx.Model.Languages.First().Language);
            }            

            await Task.Yield();
        }

        private Regex _csvParser = new Regex("(\\\")(.*?)(\\\")", RegexOptions.Compiled);

        public void LoadLanguage(SystemLanguage language)
        {            
            _keys.Clear();

            var ctx = GameState.Current.LocalizationContext;
            var languageInfo = ctx.Model.Languages.FirstOrDefault(x => x.Language == language);
            var code = languageInfo?.Code;
            if (string.IsNullOrEmpty(code))
            {
                Debug.LogError($"Unsupported language: {language}");
                return;
            }

            var data = Resources.Load<TextAsset>($"Localization/{code}");
            using (var reader = new StringReader(data.text))
            {
                var line = string.Empty;
                while ((line = reader.ReadLine()) != null)
                {
                    var matches = _csvParser.Matches(line);
                    if (matches.Count == 2)
                    {
                        _keys.Add(matches[0].Groups[2].Value,
                            matches[1].Groups[2].Value
                                .Replace("\\n", "\n")
                                .Replace("\\t", "\t"));
                    }
                }
            }
            
            ctx.CurrentLanguage = languageInfo;
        }

        public string Get(string key, params object[] args)
        {
            if (_keys.ContainsKey(key))
            {
                return ReplaceTags(string.Format(_keys[key], args))
//                    .Replace("color=green", "color=#348608")
                    ;
            }

            return key;
        }

        public string ReplaceTags(string input)
        {
            return Regex.Replace(input, @"\[(\w+)\]", (match) =>
            {
                var index = Array.IndexOf(_tags, match.Groups[1].Value);
                if (index < 0)
                {
                    return match.Value;
                }

                return $"<sprite={index}>";
            })
//            .Replace("color=green", "color=#348608")
            ;
        }

        public string ReplaceNumber(float input)
        {
            if (input >= 100f)
            {
                return Mathf.FloorToInt(input).ToString();
            }

            return input.ToString("0.##");
        }
    }
}