using System;
using System.Threading.Tasks;
using Assets.Game.Core.Common;
using Game.Scripts.Domain;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Assets.Game.Domain.Systems
{
    public class LoadingSceneSystem : ContextSystem
    {
        private bool _isClicked = false;

        public override Task UpdateAsync()
        {
            var ctx = LoadingState.Current.LoadingSceneContext;
            if (Input.GetMouseButtonUp(0) && ctx.LoadingProgress >= 1f)
            {
                _isClicked = true;
            }

            return base.UpdateAsync();
        }

        public void ResetLoadingContext()
        {
            _isClicked = false;
            var ctx = LoadingState.Current.LoadingSceneContext;
            ctx.SceneLoadingAlpha = 1f;
            ctx.IsLoadingDone = false;
            ctx.LoadingProgress = 0f;
            ctx.UpdateViews();
        }
        
        public async Task LoadEmptyAsync()
        {
            var elapsedTime = 0f;
            var ctx = LoadingState.Current.LoadingSceneContext;
            var duration = ctx.Model.LoadDurationSec;
            
            while (ctx.LoadingProgress < 1f)
            {
                ctx.LoadingProgress = Mathf.Clamp01(elapsedTime / duration);
                elapsedTime += Time.deltaTime;

                ctx.UpdateViews();
                await Task.Yield();
            }

            ctx.LoadingProgress = 1f;
            ctx.UpdateViews();
            
            while (!_isClicked)
            {
                await Task.Yield();
            }
            
            OnSceneLoaded(null);
        }

        public async Task LoadSceneAsync(string sceneName)
        {
            if (SceneManager.GetSceneByName(sceneName).IsValid())
            {
                SceneManager.UnloadSceneAsync(sceneName);
            }
            
            // TODO: wrap with Awaitable or UniTask-like extension
            AsyncOperation operation = SceneManager.LoadSceneAsync(sceneName, LoadSceneMode.Additive);
            operation.allowSceneActivation = true;

            var elapsedTime = 0f;
            var ctx = LoadingState.Current.LoadingSceneContext;
            var duration = ctx.Model.LoadDurationSec;
            while (ctx.LoadingProgress < 1f)
            {
                ctx.LoadingProgress = Mathf.Clamp01(elapsedTime * operation.progress / (0.9f * duration));
                elapsedTime += Time.deltaTime;

                ctx.UpdateViews();
                await Task.Yield();
            }
            
            ctx.LoadingProgress = 1f;
            ctx.UpdateViews();

            while (!_isClicked)
            {
                await Task.Yield();
            }

            operation.allowSceneActivation = true;
            OnSceneLoaded(sceneName);
        }
        
        private void OnSceneLoaded(string sceneName)
        {
            if (string.IsNullOrEmpty(sceneName) || sceneName == GameState.GameSceneName)
            {
                UIEvents.OnGameSceneLoaded?.Invoke();
            }
        }
    }
}