﻿using System;
using Assets.Extensions;
using Game.Scripts.Domain;
using Game.Scripts.Domain.System;
using Game.Scripts.Domain.View;
using Game.Scripts.Domain.View.Screens;
using UnityEngine.AddressableAssets;
using UnityEngine.UI;

namespace Assets.Game.Domain
{
    using System.Threading.Tasks;
    using Systems;
    using Contexts;
    using UnityEngine;
    using Models;
    using Assets.Game.Core.Common;
    using Assets.Game.Domain.Views;

    public partial class GameScene : MonoBehaviour
    {
        public static GameScene Instance { get; private set; }
        
        [Header("Models")]
        [SerializeField] private GameModel _defaultGameModel;

        [Header("Views")]
        [SerializeField] private GoldView _goldView;
        [SerializeField] private EnergyView _energyView;
        [SerializeField] private ChangeDeckView _changeDeckView;
        [SerializeField] private InGameMenuView _inGameMenuView;
        
        [Header("Screens")]
        [SerializeField] private LockScreenView _lockScreen;
        [SerializeField] private GameObject _gameOverScreen;
        [SerializeField] private Button _exitGameButton;
        [SerializeField] private Button _restartGameButton;
        
        [Header("HUD")]
        [SerializeField] private Transform _handRoot;
        [SerializeField] private Transform _farmRoot;
        [SerializeField] private Transform _eventsRoot;
        [SerializeField] private Button _nextTurnButton;

        private Screens _screens;

        public async void Start()
        {
            Instance = this;
            
            _screens = new Screens();
            _screens.Setup();
                
            // Создаем новое состояние игры
            var isNewGame = GameState.Current == null;
            if (isNewGame)
            {
                GameState.Current = new GameState();
                GameState.Current.GameContext = ContextSystem.CreateContext<GameContext, GameModel>(_defaultGameModel);
                
                await ContextSystem.GetOrCreate<LocalizationSystem>().SetupAsync();
                await ContextSystem.GetOrCreate<DeckSystem>().SetupAsync();
                await ContextSystem.GetOrCreate<GameEventSystem>().SetupAsync();
                await SetupNewGame();
            }
            
            await SetupUI();
            
            await ContextSystem.GetOrCreate<TurnSystem>().NextTurn();
            Debug.Log("GameScene initialized");
        }

        private void Update()
        {
            _screens?.Update();
        }

        private async Task SetupNewGame()
        {
            var game = GameState.Current.GameContext;
            game.Gold = 0;
            game.MaxEnergy = game.Model.BasicEnergy;
            
            await Task.Yield();
        }

        private async Task SetupUI()
        {
            _inGameMenuView.Attach(GameState.Current.GameContext);
            _changeDeckView.Setup();
            _goldView.Attach(GameState.Current.GameContext);
            _energyView.Attach(GameState.Current.GameContext);

            await SetupCardsUI();
            await SetupFarmUI();
            await SetupEventsUI();
            
            _nextTurnButton.onClick.RemoveAllListeners();
            _nextTurnButton.onClick.AddListener(() => DoNextTurn());
            
            _exitGameButton.onClick.RemoveAllListeners();
            _exitGameButton.onClick.AddListener(() => ExitGame());
            
            _restartGameButton.onClick.RemoveAllListeners();
            _restartGameButton.onClick.AddListener(() => RestartGame());
            
            _gameOverScreen.SetActive(false);
        }

        private async Task SetupCardsUI()
        {
            var handle = Addressables.LoadAssetAsync<GameObject>("CardView");
            var prefab = await handle.Task;
            for (int i = 0; i < GameState.Current.Cards.Count; i++)
            {
                var view = Instantiate(prefab, _handRoot).GetComponent<CardView>();
                view.Attach(GameState.Current.Cards[i]);
            }
            Addressables.Release(handle);
        }
        
        private async Task SetupEventsUI()
        {
            var handle = Addressables.LoadAssetAsync<GameObject>("GameEventView");
            var prefab = await handle.Task;
            for (int i = 0; i < GameState.Current.Events.Count; i++)
            {
                var view = Instantiate(prefab, _eventsRoot).GetComponent<GameEventView>();
                view.Attach(GameState.Current.Events[i]);
            }
            Addressables.Release(handle);
        }
        
        private async Task SetupFarmUI()
        {
            _farmRoot.DestroyChildren();
            
            var handle = Addressables.LoadAssetAsync<GameObject>("FarmSlot");
            var prefab = await handle.Task;
            var farmSize = GameState.Current.GameContext.Model.FarmSize;
            for (int i = 0; i < farmSize; i++)
            {
                var view = Instantiate(prefab, _farmRoot).GetComponent<FarmSlotView>();
                view.ClearSlot();
            }
            Addressables.Release(handle);
        }
        
        private async void DoNextTurn()
        {
            await ContextSystem.GetOrCreate<TurnSystem>().NextTurn();
        }

        public T GetView<T>() where T : IContextView
        {
            if (typeof(T) == typeof(InGameMenuView))
            {
                return (T)(object)_inGameMenuView;
            }
            
            if (typeof(T) == typeof(LockScreenView))
            {
                return (T)(object)_lockScreen;
            }

            return default(T);
        }
    }
}
