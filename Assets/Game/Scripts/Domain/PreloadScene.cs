﻿using System.Threading.Tasks;
using Assets.Game.Core.Common;
using Assets.Game.Domain.Views;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Assets.Game.Domain
{
    public static class PreloadScene
    {
        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
        public static void InitializeBeforeSceneLoad()
        {
            Debug.Log("[Preload] Before scene load");
            SceneManager.LoadScene(LoadingState.LoadingSceneName, LoadSceneMode.Additive);         
        }

        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.AfterSceneLoad)]
        public static void InitializeAfterSceneLoad()
        {
            Debug.Log("[Preload] After scene load");
            SceneManager.sceneLoaded += OnSceneLoaded;
        }

        private static void OnSceneLoaded(Scene scene, LoadSceneMode loadSceneMode)
        {
            Debug.Log("[Preload] Scene loaded");
            SceneManager.sceneLoaded -= OnSceneLoaded;
        }
    }
}
