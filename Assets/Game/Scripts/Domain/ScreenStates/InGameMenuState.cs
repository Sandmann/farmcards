using Assets.Game.Core.Common;
using Assets.Game.Domain;
using Assets.Game.Domain.Systems;
using Assets.Game.Domain.Views;
using Game.Scripts.Core;
using UnityEngine;

namespace Game.Scripts.Domain.ScreenStates
{
    public class InGameMenuState : ScreenState
    {
        public InGameMenuState()
        {
            UIEvents.ExitGame += OnExitGame;
            UIEvents.SaveGame += OnSaveGame;
            UIEvents.LoadGame += OnLoadGame;
        }
        
        public override void OnEnter()
        {
            var view = GameScene.Instance.GetView<InGameMenuView>();
            if (view)
            {
                view.SetVisible(true);
            }
        }

        public override void OnExit()
        {
            var view = GameScene.Instance.GetView<InGameMenuView>();
            if (view)
            {
                view.SetVisible(false);
            }
        }

        public override void OnUpdate()
        {
            if (Input.GetKeyUp(KeyCode.Escape))
            {
                UIEvents.HideInGameMenu?.Invoke();
            }
        }
        
        private void OnExitGame()
        {
            GameScene.Instance.ExitGame();
        }
        
        private void OnSaveGame()
        {
            ContextSystem.GetOrCreate<SaveLoadSystem>().SaveGame();            
        }

        private async void OnLoadGame()
        {
            ContextSystem.GetOrCreate<SaveLoadSystem>().LoadGame("save");
            await LoadingScene.Instance.LoadGameScene();
        }
    }
}
