using Assets.Game.Domain;
using Game.Scripts.Core;
using Game.Scripts.Domain.View;

namespace Game.Scripts.Domain.ScreenStates
{
    public class GameSceneLoadingState : ScreenState
    {
        public override void OnEnter()
        {
            var view = GameScene.Instance.GetView<LockScreenView>();
            if (view)
            {
                view.SetVisible(true);
            }
        }

        public override void OnExit()
        {
            var view = GameScene.Instance.GetView<LockScreenView>();
            if (view)
            {
                view.SetVisible(false);
            }
        }
    }
}