using Game.Scripts.Core;
using UnityEngine;

namespace Game.Scripts.Domain.ScreenStates
{
    public class GameplayState : ScreenState
    {
        public override void OnUpdate()
        {
            if (Input.GetKeyUp(KeyCode.Escape))
            {
                UIEvents.ShowInGameMenu?.Invoke();
            }
        }
    }
}
