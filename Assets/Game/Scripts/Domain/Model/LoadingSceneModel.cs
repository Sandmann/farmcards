using Assets.Game.Core.Common;
using UnityEngine;

namespace Assets.Game.Domain.Models
{
    [CreateAssetMenu(fileName = "LoadingSceneModel", menuName = "Data/LoadingSceneModel")]
    public class LoadingSceneModel : ContextModel
    {
        public float FadeDurationSec = 1f;
        public float LoadDurationSec = 2f;
    }
}