using Assets.Game.Core.Common;
using UnityEngine;

namespace Assets.Game.Domain.Models
{
    public enum CropsEffect
    {
        None = 0,
    }
    
    [CreateAssetMenu(fileName = "CropsModel", menuName = "Data/CropsModel")]
    public class CropsModel : ContextModel
    {
        public Sprite Icon;
        public int WaterOnStart;
        public int LifeTime;
        public int GoldReward;
    }
}