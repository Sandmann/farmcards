using Assets.Game.Core.Common;

namespace Assets.Game.Domain.Models
{
    using System;
    using UnityEngine;

    [CreateAssetMenu(fileName = "LocalizationModel", menuName = "Data/LocalizationModel")]
    public class LocalizationModel : ContextModel
    {
        public LanguageInfo[] Languages = new LanguageInfo[0];
    }

    [Serializable]
    public class LanguageInfo
    {
        public string Code;
        public SystemLanguage Language;
        public Sprite Flag;
    }
}
