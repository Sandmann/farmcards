using Assets.Game.Core.Common;
using UnityEngine;

namespace Assets.Game.Domain.Models
{
    public enum CardZone : byte
    {
        None = 0,
        Deck = 1,
        Hand = 2,
        Discard = 3,
        Removed = 4,
    }

    public enum CardEffect
    {
        None = 0,
        AddWaterToSingleCrops = 1,
    }
    
    [CreateAssetMenu(fileName = "CardModel", menuName = "Data/CardModel")]
    public class CardModel : ContextModel
    {
        public int EnergyCost;
        public Sprite Icon;
        public CropsModel CropsModel;
        public int EffectValue;
        public CardEffect Effect = CardEffect.None;
    }
}