using Assets.Game.Core.Common;
using UnityEngine;

namespace Assets.Game.Domain.Models
{
    [CreateAssetMenu(fileName = "HandModel", menuName = "Data/HandModel")]
    public class HandModel : ContextModel
    {
        
    }
}