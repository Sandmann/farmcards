using System;
using Assets.Game.Core.Common;

namespace Assets.Game.Domain.Models
{
    using UnityEngine;

    [CreateAssetMenu(fileName = "GameModel", menuName = "Data/GameModel")]
    public class GameModel : ContextModel
    {
        public int BasicEnergy;
        public int HandSize;
        public int FarmSize;
        public int EventSlotCount = 12;

        public int BasicRent = 12;

        public CardModel[] EventsCard = Array.Empty<CardModel>();
        public CardModel[] StartingDeck = Array.Empty<CardModel>();
        public GameEventModel[] Events = Array.Empty<GameEventModel>();
    }
}
