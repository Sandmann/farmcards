using Assets.Game.Core.Common;

namespace Assets.Game.Domain.Models
{
    using UnityEngine;

    [CreateAssetMenu(fileName = "FieldModel", menuName = "Data/FieldModel")]
    public class FieldModel : ContextModel
    {
    }
}
