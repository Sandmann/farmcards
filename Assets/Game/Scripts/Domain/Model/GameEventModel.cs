using Assets.Game.Core.Common;
using UnityEngine;

namespace Assets.Game.Domain.Models
{
    public enum GameEvent : byte
    {
        None = 0,
        RentCollect = 1,
        AddNewCards = 2,
        RemoveCards = 3,
    }
    
    [CreateAssetMenu(fileName = "GameEventModel", menuName = "Data/GameEventModel")]
    public class GameEventModel : ContextModel
    {
        public Sprite Icon;
        public GameEvent Type;
        [TextArea] public string ToolTipText;
    }
}