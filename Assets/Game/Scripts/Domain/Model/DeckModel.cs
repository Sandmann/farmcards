using Assets.Game.Core.Common;
using UnityEngine;

namespace Assets.Game.Domain.Models
{
    [CreateAssetMenu(fileName = "DeckModel", menuName = "Data/DeckModel")]
    public class DeckModel : ContextModel
    {
        
    }
}