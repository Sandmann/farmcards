using Assets.Game.Domain.Contexts;

namespace Assets.Game.Domain
{
    public class LoadingState
    {
        public static LoadingState Current { get; set; }
        
        public const string LoadingSceneName = "Loading";

        public LoadingSceneContext LoadingSceneContext = null;
    }
}