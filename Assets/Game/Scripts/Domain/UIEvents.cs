using System;

namespace Game.Scripts.Domain
{
    public static class UIEvents
    {
        public static Action ShowInGameMenu = delegate { };
        public static Action HideInGameMenu = delegate { };
        
        public static Action ExitGame = delegate { };
        public static Action SaveGame = delegate { };
        public static Action LoadGame = delegate { };
        
        public static Action OnGameSceneLoaded = delegate { };
    }
}