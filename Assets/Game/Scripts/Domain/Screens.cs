using Game.Scripts.Core;
using Game.Scripts.Domain.ScreenStates;

namespace Game.Scripts.Domain
{
    public class Screens
    {
        private ScreenState _gameplayScreenState;
        private ScreenState _inGameMenuState;
        private ScreenState _loadingGameSceneState;

        private ScreenState _current;

        public void Setup()
        {
            _inGameMenuState = new InGameMenuState();
            _gameplayScreenState = new GameplayState();
            _loadingGameSceneState = new GameSceneLoadingState();
            
            _gameplayScreenState.AddLink(new ScreenLink(ref UIEvents.ShowInGameMenu, _inGameMenuState));
            _inGameMenuState.AddLink(new ScreenLink(ref UIEvents.HideInGameMenu, _gameplayScreenState));
            _loadingGameSceneState.AddLink(new ScreenLink(ref UIEvents.OnGameSceneLoaded, _gameplayScreenState));
            
            SetState(_loadingGameSceneState);
        }

        public void Update()
        {
            _current?.OnUpdate();
            
            if (_current?.ValidateLinks(out var targetState) == true)
            {
                SetState(targetState);
            }
        }

        private void SetState(ScreenState state)
        {
            _current?.OnExit();

            _current = state;
            _current.OnEnter();
        }
    }
}
