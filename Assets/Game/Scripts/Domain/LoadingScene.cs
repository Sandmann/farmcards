using System;
using System.Threading.Tasks;
using Assets.Game.Core.Common;
using Assets.Game.Domain.Contexts;
using Assets.Game.Domain.Models;
using Assets.Game.Domain.Systems;
using Assets.Game.Domain.Views;
using UnityEngine;

namespace Assets.Game.Domain
{
    public class LoadingScene : MonoBehaviour
    {
        public static LoadingScene Instance { get; private set; }
        [Header("Views")]
        [SerializeField] private LoadSceneView _loadSceneView;

        private async void Start()
        {
            Instance = this;
            
            if (LoadingState.Current == null)
            {
                LoadingState.Current = new LoadingState();
                LoadingState.Current.LoadingSceneContext =
                    await ContextSystem.CreateContextAsync<LoadingSceneContext, LoadingSceneModel>("LoadingSceneModel");
            }
            
            _loadSceneView.Attach(LoadingState.Current.LoadingSceneContext);

            await EmulateSceneLoading();
            Debug.Log($"LoadingScene finished");
        }
        
        private void Update()
        {
            ContextSystem.GetOrCreate<LoadingSceneSystem>().UpdateAsync();
        }

        private async Task EmulateSceneLoading()
        {
            var loading = ContextSystem.GetOrCreate<LoadingSceneSystem>();
            loading.ResetLoadingContext();
            await loading.LoadEmptyAsync();
            await _loadSceneView.FadeAsync(1f, 0f, 1f);
        }

        public async Task LoadGameScene()
        {
            var loading = ContextSystem.GetOrCreate<LoadingSceneSystem>();
            loading.ResetLoadingContext();
            await _loadSceneView.FadeAsync(0f, 1f, 1f);
            
            await loading.LoadSceneAsync(GameState.GameSceneName);
            await _loadSceneView.FadeAsync(1f, 0f, 1f);
        }
        
    }
}