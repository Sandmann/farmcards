using Assets.Game.Core.Common;
using Assets.Game.Domain.Contexts;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using NotImplementedException = System.NotImplementedException;

namespace Game.Scripts.Domain.View
{
    public class GameEventView : ContextView<GameEventContext>, IPointerEnterHandler, IPointerExitHandler
    {
        [SerializeField] private Image _icon;
        [Header("ToolTip")]
        [SerializeField] private TMP_Text _tooltipMessage;
        [SerializeField] private GameObject _tooltipRoot;
        [Space]
        [SerializeField] private RectTransform _root;
        [SerializeField] private int _slotSize = 120;
        
        protected override void OnAttach()
        {
            _icon.sprite = GetContext().Model.Icon;
            _tooltipRoot.SetActive(false);
        }

        protected override void OnUpdate()
        {
            _root.anchoredPosition = new Vector2(_slotSize * GetContext().Turn, 0f);
            gameObject.SetActive(GetContext().Turn > 0);
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            _tooltipMessage.text = string.Format(GetContext().Model.ToolTipText, GetContext().Value);
            _tooltipRoot.SetActive(true);
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            _tooltipRoot.SetActive(false);
        }
    }
}