using Assets.Game.Core.Common;
using Assets.Game.Domain.Contexts;
using TMPro;
using UnityEngine;

namespace Game.Scripts.Domain.View
{
    public class GoldView : ContextView<GameContext>
    {
        [SerializeField] private TMP_Text _value;
        
        protected override void OnUpdate()
        {
            _value.text = $"{GetContext().Gold}";
        }
    }
}
