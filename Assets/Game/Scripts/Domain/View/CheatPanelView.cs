﻿namespace Assets.Game.Domain.Systems
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Core.Common;
    using Models;
    using TMPro;
    using UnityEngine;

    public class CheatPanelView : MonoBehaviour
    {
        [SerializeField] private Texture2D _background;
        [SerializeField] private TextMeshProUGUI _log;
        [SerializeField] private Color _backgroundColor = new Color(0f, 0f, 0f, 0.7f);

        private bool _showPanel;
        private int _tabIndex;
        private string _text = "100";
        private bool _toggle;
        private float _slider = 0.5f;

        private Rect _windowRect = new Rect(0, 100f, 300, 300);
        private GUIStyle _windowStyle = new GUIStyle();

        private List<string> _logs = new List<string>();

        protected void Start()
        {
            _windowStyle.normal.background = _background;
        }

        protected void OnEnable()
        {
            _log.text = string.Empty;
            _logs.Clear();
            Application.logMessageReceived += HandleLog;
        }

        protected void OnDisable()
        {
            Application.logMessageReceived -= HandleLog;
        }

        protected void Update()
        {
            if (Input.GetKeyUp(KeyCode.BackQuote))
            {
                _showPanel = !_showPanel;
            }
        }

        protected void OnGUI()
        {
            if (!_showPanel)
            {
                return;
            }

            GUI.backgroundColor = _backgroundColor;
            _windowRect = GUILayout.Window(0, _windowRect, DrawCheats, "Hello", _windowStyle);
        }
        
        private void HandleLog(string logString, string stackTrace, LogType type)
        {
            _logs.Add(logString);
            if (_logs.Count > 15)
            {
                _logs.RemoveAt(0);
            }

            _log.text = string.Join("\n", _logs);
        }

        private void DrawCheats(int windowId)
        {
            GUILayout.BeginHorizontal();
            GUILayout.Space(20);
            
            GUILayout.BeginVertical();
            GUILayout.Space(20);

            // Tab pages
            GUILayout.BeginHorizontal();
            GUILayout.Label("", GUILayout.Width(100f));
            if (GUILayout.Button("Cheats"))
            {
                _tabIndex = 0;
            }
            GUILayout.EndHorizontal();

            // Line space
            GUILayout.BeginVertical();
            GUILayout.Space(10);
            GUILayout.EndVertical();

            // Cheats
            if (_tabIndex == 0)
            {
                // TODO: Cheat button templates. Add new cheats there...
                GUILayout.BeginHorizontal();                

                if (GUILayout.Button("Explore map"))
                {
                    Debug.Log($"Map explored");
                }

                GUILayout.EndHorizontal();

                if (GUILayout.Button("Build all"))
                {
                    Debug.Log($"Build completed.");
                }

                // Slider example
                GUILayout.Label($"Music volume: {100f * _slider}");
                _slider = GUILayout.HorizontalSlider(_slider, 0f, 1f, GUILayout.Width(200f));

                GUILayout.BeginHorizontal();

                // Field example
                GUILayout.Label("Workers: ");
                _text = GUILayout.TextField(_text, GUILayout.Width(60f));
                if (GUILayout.Button("Создать"))
                {
                    Debug.Log(_text);
                }

                if (GUILayout.Button("Kill worker"))
                {
                    Debug.Log("Reset");
                }

                GUILayout.EndHorizontal();

                // Toggle example
                GUILayout.BeginVertical();
                _toggle = GUILayout.Toggle(_toggle, "Enable mode 2");
                _toggle = GUILayout.Toggle(_toggle, "Enable mode 3");
                GUILayout.Label("Misc");
                GUILayout.EndVertical();
            }

            GUILayout.EndVertical();
            GUILayout.EndHorizontal();
        }
    }
}
