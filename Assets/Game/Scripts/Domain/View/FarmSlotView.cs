using Assets.Game.Core.Common;
using Assets.Game.Domain;
using Assets.Game.Domain.Contexts;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using NotImplementedException = System.NotImplementedException;

namespace Game.Scripts.Domain.View
{
    public class FarmSlotView : ContextView<CropsContext>, IDropHandler, IPointerClickHandler
    {
        [SerializeField] private GameObject _cropsRoot;
        [SerializeField] private GameObject _cropsStats;
        [SerializeField] private TMP_Text _cropsStatus;
        [SerializeField] private TMP_Text _cropsName;
        [SerializeField] private Image _cropsIcon;
        [SerializeField] private TMP_Text _waterValue;
        [SerializeField] private TMP_Text _lifeTimeValue;

        //todo: non-optimized
        [SerializeField] private Sprite _deadCropsIcon;

        protected override void OnAttach()
        {
            _cropsName.text = GetContext().Model.Id;
            _cropsIcon.sprite = GetContext().Model.Icon;
            
            _cropsRoot.SetActive(true);
        }

        protected override void OnUpdate()
        {
            _waterValue.text = $"{GetContext().CurWater}";
            _lifeTimeValue.text = $"{GetContext().CurLifeTime}";

            _waterValue.color = GetContext().CurWater >= GetContext().CurLifeTime
                ? Color.blue
                : Color.red;
            
            _cropsStats.SetActive(GetContext().CanCollect == false);
            _cropsStatus.text = string.Empty;
            if (GetContext().IsRipe)
            {
                _cropsStatus.text = $"Ripe";
                _cropsStatus.color = Color.green;
            }
            if (GetContext().IsDead)
            {
                _cropsStatus.text = $"Dead";
                _cropsStatus.color = Color.red;
                _cropsIcon.sprite = _deadCropsIcon;
            }
        }

        protected override void OnDetach()
        {
            ClearSlot();
        }

        public void ClearSlot()
        {
            _cropsName.text = string.Empty;
            _cropsRoot.SetActive(false);
        }

        public void OnDrop(PointerEventData eventData)
        {
            var cardView = eventData.pointerDrag.GetComponent<CardView>();
            if (cardView)
            {
                if (GameScene.Instance.TryApplyCardToSlot(this, cardView.GetContext()))
                {
                    cardView.OnEndDrag(eventData);
                }
            }
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            if (GetContext()?.CanCollect == true)
            {
                GameScene.Instance.CollectCropsFromSlot(this);
            }
        }
    }
}
