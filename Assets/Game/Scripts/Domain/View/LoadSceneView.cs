﻿
using Assets.Game.Core.Common;
using Assets.Game.Domain.Contexts;
using System.Collections;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Game.Domain.Views
{
    public class LoadSceneView : ContextView<LoadingSceneContext>
    {
        [SerializeField] private Image _progressBar;
        [SerializeField] private Text _progressBarText;
        [SerializeField] private CanvasGroup _canvasGroup;

        private bool _isFaiding;

        protected override void OnAttach()
        {
            UpdateProgressBar();
            SetVisible(true);
        }

        protected override void OnUpdate()
        {
            UpdateProgressBar();
        }

        private void UpdateProgressBar()
        {
            var progress = GetContext().LoadingProgress;
            _progressBar.fillAmount = progress;
            _progressBarText.text = progress >= 1f
                ? "Click anywhere to start."
                : $"Loading... {(int)(progress * 100f)}%";
            
            _canvasGroup.alpha = GetContext().SceneLoadingAlpha;
        }

        private void SetVisible(bool value)
        {
            _canvasGroup.blocksRaycasts = value;
            _canvasGroup.gameObject.SetActive(value);
            gameObject.SetActive(value);
        }
        
        public async Task FadeAsync(float from, float to, float duration)
        {
            _canvasGroup.alpha = from;
            SetVisible(true);
            
            float fadeSpeed = Mathf.Abs(from - to) / duration;
            while (!Mathf.Approximately(_canvasGroup.alpha, to))
            {
                _canvasGroup.alpha = Mathf.MoveTowards(_canvasGroup.alpha, to,
                    fadeSpeed * Time.deltaTime);
                
                await Task.Yield();
            }

            SetVisible(to > 0f);
        }
    }
}
