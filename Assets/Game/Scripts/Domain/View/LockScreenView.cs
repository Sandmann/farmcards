using Assets.Game.Core.Common;
using Assets.Game.Domain.Contexts;

namespace Game.Scripts.Domain.View
{
    public class LockScreenView : ContextView<GameContext>
    {
        public void SetVisible(bool value)
        {
            gameObject.SetActive(value);
        }
    }
}