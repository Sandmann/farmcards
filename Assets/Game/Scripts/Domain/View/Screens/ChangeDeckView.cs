using System.Threading.Tasks;
using Assets.Extensions;
using Assets.Game.Domain;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using ScreenState = Game.Scripts.Core.ScreenState;

namespace Game.Scripts.Domain.View.Screens
{
    public class ChangeDeckView : MonoBehaviour
    {
        [SerializeField] private TMP_Text _title;
        [SerializeField] private Button _skipButton;
        [SerializeField] private Transform _cardsRoot;

        public Transform CardsRoot => _cardsRoot;
        
        public void Setup()
        {
            _skipButton.onClick.RemoveAllListeners();
            _skipButton.onClick.AddListener(() => Hide());
            
            Dispose();
        }

        public async Task ShowAddCardIntoDeck()
        {
            _title.text = "Select a card to put into the deck";
            // await SetScreenStateAsync(ScreenStates.SelectCardToAdd);
        }
        public async Task ShowRemoveCardFromDeck()
        {
            _title.text = "Select a card to remove from_ the deck";
            // await SetScreenStateAsync(ScreenStates.SelectCardToRemove);
        }

        // private async Task SetScreenStateAsync(ScreenStates newState)
        // {
        //     ScreenState.Current = newState;
        //     gameObject.SetActive(true);
        //
        //     //TODO: state machine has more control; refactor to ScreenState
        //     //this blocks NextTurn flow to wait reaction from player and to resume after that
        //     while (ScreenState.Current == newState)
        //     {
        //         await Task.Delay(100);
        //     }
        // }

        public void Hide()
        {
            // ScreenState.Current = ScreenStates.Gameplay;
            Dispose();
        }

        private void Dispose()
        {
            _cardsRoot.DestroyChildren();
            gameObject.SetActive(false);
        }
    }
}
