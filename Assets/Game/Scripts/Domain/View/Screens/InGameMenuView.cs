

using Assets.Game.Domain.Contexts;
using Game.Scripts.Domain;

namespace Assets.Game.Domain.Views
{
    using Assets.Game.Core.Common;
    using Assets.Game.Domain.Systems;
    using TMPro;
    using UnityEngine;
    using UnityEngine.UI;

    public class InGameMenuView : ContextView<GameContext>
    {
        [SerializeField] private TextMeshProUGUI _title;

        [SerializeField] private Button _resumeButton;
        [SerializeField] private Button _saveGameButton;
        [SerializeField] private Button _loadGameButton;
        [SerializeField] private Button _exitGameButton;

        protected override void OnAttach()
        {
            _title.text = ContextSystem.GetOrCreate<LocalizationSystem>().Get("ui_ingamemenu_title");

            _resumeButton.onClick.RemoveAllListeners();
            _resumeButton.onClick.AddListener(Resume);

            _saveGameButton.onClick.RemoveAllListeners();
            _saveGameButton.onClick.AddListener(SaveGame);

            _loadGameButton.onClick.RemoveAllListeners();
            _loadGameButton.onClick.AddListener(LoadGame);

            _exitGameButton.onClick.RemoveAllListeners();
            _exitGameButton.onClick.AddListener(ExitGame);
            
            SetVisible(false);
        }

        public void SetVisible(bool value)
        {
            gameObject.SetActive(value);
        }
        
        private void Resume()
        {
            UIEvents.HideInGameMenu?.Invoke();
        }

        private void ExitGame()
        {
            UIEvents.ExitGame?.Invoke();
        }

        private void SaveGame()
        {
            UIEvents.SaveGame?.Invoke();            
        }

        private void LoadGame()
        {
            UIEvents.LoadGame?.Invoke();
        }
    }
}