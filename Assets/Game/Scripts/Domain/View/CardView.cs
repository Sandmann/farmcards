using System;
using Assets.Game.Core.Common;
using Assets.Game.Domain;
using Assets.Game.Domain.Contexts;
using Assets.Game.Domain.Models;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Game.Scripts.Domain.View
{
    public enum CardViewMode
    {
        Gameplay = 0,
        Selector = 1,
    }
    
    public class CardView : ContextView<CardContext>, IBeginDragHandler, IDragHandler, IEndDragHandler, IPointerClickHandler
    {
        [SerializeField] private TMP_Text _name;
        [SerializeField] private TMP_Text _effect;
        
        [SerializeField] private TMP_Text _energy;
        [SerializeField] private TMP_Text _lifeTime;
        [SerializeField] private TMP_Text _gold;
        
        [SerializeField] private Image _icon;
        [SerializeField] private Graphic _root;

        private Vector3 _startPos;
        private RectTransform _transform;

        public CardViewMode ViewMode = CardViewMode.Gameplay;

        private void Awake()
        {
            _transform = GetComponent<RectTransform>();
        }

        protected override void OnAttach()
        {
            var cardModel = GetContext().Model;
            var cropsModel = GetContext().Model.CropsModel;
            _icon.sprite = cropsModel
                ? cropsModel.Icon
                : cardModel.Icon;

            _name.text = cardModel.Id;
            _energy.text = $"{cardModel.EnergyCost}";

            if (cropsModel)
            {
                _lifeTime.text = $"{cropsModel.LifeTime}";
                _lifeTime.gameObject.SetActive(true);
            
                var gold = cropsModel.GoldReward;
                _gold.text = $"{gold}";
                _gold.gameObject.SetActive(gold > 0);
            }
            else
            {
                _lifeTime.gameObject.SetActive(false);
                _gold.gameObject.SetActive(false);
            }

            _effect.text = $"Add crops to empty slot";
            if (cardModel.Effect == CardEffect.AddWaterToSingleCrops)
            {
                _effect.text = $"Add {cardModel.EffectValue} water to crops in slot";
            }
        }

        protected override void OnUpdate()
        {
            if (ViewMode == CardViewMode.Gameplay)
            {
                gameObject.SetActive(GetContext().Zone == CardZone.Hand);
                
                if (GetContext().Zone == CardZone.Removed)
                {
                    Destroy(gameObject);
                }
            }
        }

        public void OnBeginDrag(PointerEventData eventData)
        {
            if (ViewMode == CardViewMode.Gameplay)
            {
                _startPos = _transform.anchoredPosition;
                _root.raycastTarget = false;
            }
        }

        public void OnDrag(PointerEventData eventData)
        {
            if (ViewMode == CardViewMode.Gameplay)
            {
                _transform.anchoredPosition += eventData.delta;
            }
        }

        public void OnEndDrag(PointerEventData eventData)
        {
            if (ViewMode == CardViewMode.Gameplay)
            {
                _transform.anchoredPosition = _startPos;
                _root.raycastTarget = true;
            }
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            if (ViewMode == CardViewMode.Selector)
            {
                GameScene.Instance.SelectCardForGameEvent(this);
            }
        }
    }
}
