﻿namespace Assets.Game.Core.Common
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using UnityEngine;
    using UnityEngine.AddressableAssets;

    public interface IContext
    {
        void AttachView(IContextView view);
        void DetachView(IContextView view);
        void UpdateViews();
    }
    
    [Serializable]
    public abstract class Context : IContext
    {
        public uint Id;

        private readonly List<IContextView> _subscribers = new List<IContextView>();

        public void UpdateViews()
        {
            _subscribers.RemoveAll(x => x == null || x.Equals(null));
            foreach (var s in _subscribers)
                s.UpdateState();
        }

        public void AttachView(IContextView view)
        {
            if (_subscribers.Find(v => v.Equals(view)) == null)
                _subscribers.Add(view);
        }

        public void DetachView(IContextView view)
        {
            if (_subscribers.Find(v => v.Equals(view)) != null)
                _subscribers.Remove(view);
        }

        public void DetachAll()
        {
            _subscribers.Clear();
        }
    }

    [Serializable]
    public class Context<T> : Context, ISerializationCallbackReceiver where T : ContextModel
    {
        [SerializeField]
        private string _modelId = null;

        [NonSerialized]
        public T Model;

        public void OnBeforeSerialize()
        {
            // Защита для сериализации префабов, у которых вьюхи с Context<T>
            if (Model)
            {
                _modelId = Model.name;
            }
        }

        public async void OnAfterDeserialize()
        {
            // Защита для сериализации префабов, у которых вьюхи с Context<T>
            if (string.IsNullOrEmpty(_modelId))
            {
                return;
            }

            var handle = Addressables.LoadAssetAsync<T>(_modelId);
            await handle.Task;

            Model = handle.Result;
        }
    }
}
