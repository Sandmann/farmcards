﻿using System.Collections;
using UnityEngine.AddressableAssets;

namespace Assets.Game.Core.Common
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    
    public abstract class ContextSystem
    {
        private static uint _seed = 1; // seed for context ids
        private static Dictionary<string, ContextSystem> _systems = new Dictionary<string, ContextSystem>();

        public static T GetOrCreate<T>() where T : ContextSystem, new()
        {
            var sys = Get<T>();
            if (sys == null)
            {
                sys = new T();
                Add(sys);
            }

            return sys;
        }

        private static void Add(ContextSystem sys)
        {
            var typeName = sys.GetType().Name;
            _systems[typeName] = sys;
        }

        private static T Get<T>() where T : ContextSystem
        {
            var typeName = typeof(T).Name;
            if (!_systems.ContainsKey(typeName))
            {
                return default;
            }

            return _systems[typeName] as T;
        }

        public virtual async Task UpdateAsync()
        {
            await Task.Yield();
        }

        public virtual async Task SetupAsync()
        {
            await Task.Yield();
        }
        
        public static TContext CreateContext<TContext, TModel>(TModel model) where TContext : Context, new() where TModel: ContextModel
        {
            var ctx = (Context<TModel>)(object)CreateContext<TContext>();
            ctx.Model = model;

            return (TContext)(object)ctx;
        }

        public static async Task<TContext> CreateContextAsync<TContext, TModel>(string assetId) where TContext : Context, new() where TModel: ContextModel
        {
            var handle = Addressables.LoadAssetAsync<TModel>(assetId);
            await handle.Task;

            var ctx = (Context<TModel>)(object)CreateContext<TContext>();
            ctx.Model = handle.Result;

            return (TContext)(object)ctx;
        }
        
        // public async Task CreateContextAllAsync<TContext, TModel>(string groupLabel) where TContext : Context, new() where TModel: ContextModel
        // {
        //     var handle = Addressables.LoadAssetsAsync<TModel>(groupLabel, (model) =>
        //     {
        //         var ctx = (Context<TModel>)(object)CreateContext<TContext>();
        //         ctx.Model = model;
        //     });
        //
        //     await handle.Task;
        // }

        private static T CreateContext<T>() where T : Context, new()
        {
            var ctx = new T()
            {
                Id = _seed++,
            };

            // Add context into State
            // var fields = GetType().GetFields();
            // for (int i = 0; i < fields.Length; i++)
            // {
            //     if (fields[i].FieldType.IsGenericType && fields[i].FieldType.GetGenericArguments()[0] == typeof(T))
            //     {
            //         ((IList) fields[i].GetValue(this)).Add(ctx);
            //     }
            //
            //     if (fields[i].FieldType == typeof(T))
            //     {
            //         fields[i].SetValue(this, ctx);
            //     }
            // }

            return ctx;
        }

        // public async Task<T> LoadModelAsync<T>(string assetId) where T: ContextModel
        // {
        //     var handle = Addressables.LoadAssetAsync<T>(assetId);
        //     await handle.Task;
        //
        //     return handle.Result;
        // }
        //
        // public async Task<IList<T>> LoadModelAllAsync<T>(string groupId) where T: ContextModel
        // {
        //     var handle = Addressables.LoadAssetsAsync<T>(groupId, o => {});
        //     await handle.Task;
        //
        //     return handle.Result;
        // }
    }
}
