using System;

namespace Game.Scripts.Core
{
    public class ScreenLink
    {
        private bool _isValid;
        public ScreenState TargetState { get; }
        
        public ScreenLink(ref Action onEvent, ScreenState targetState)
        {
            onEvent += OnEventInvoked;
            TargetState = targetState;
        }

        public bool IsValid()
        {
            var isValid = _isValid;
            _isValid = false;
            
            return isValid;
        }

        private void OnEventInvoked()
        {
            _isValid = true;
        }
    }
}
