using System.Collections.Generic;

namespace Game.Scripts.Core
{
    public abstract class ScreenState
    {
        private List<ScreenLink> _links = new List<ScreenLink>();
        
        public void AddLink(ScreenLink link)
        {
            _links.Add(link);
        }
        
        public virtual void OnEnter()
        {
        }

        public virtual void OnExit()
        {
        }

        public virtual void OnUpdate()
        {
        }

        public bool ValidateLinks(out ScreenState state)
        {
            state = null;
            
            foreach (var link in _links)
            {
                if (link.IsValid())
                {
                    state = link.TargetState;
                    return true;
                }
            }

            return false;
        }
    }
}
