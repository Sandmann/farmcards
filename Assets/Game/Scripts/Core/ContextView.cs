﻿using UnityEngine;

namespace Assets.Game.Core.Common
{
    public interface IContextView
    {
        void Attach(IContext context);
        void MarkDirty();
        void UpdateState();
    }

    public abstract class ContextView<T> : MonoBehaviour, IContextView where T : class, IContext
    {
        private T _context;
        public T GetContext() { return _context; }

        protected bool IsDirty;
        public void MarkDirty()
        {
            IsDirty = true;

            // Ability to update state if inactive
            if (gameObject && !gameObject.activeSelf)
                UpdateState();
        }

        protected virtual void Update()
        {
            if (IsDirty)
            {
                UpdateState();
                IsDirty = false;
            }
        }

        protected virtual void OnDestroy()
        {
            _context?.DetachView(this);
        }

        public void UpdateState()
        {
            if (_context != null)
                OnUpdate();
        }

        /// <summary>
        /// Attach view to context
        /// </summary>
        /// <param name="context"></param>
        public void Attach(IContext context)
        {
            Detach();
            
            _context = (T)context;
            _context.AttachView(this);
            
            OnAttach();
            MarkDirty();
        }

        /// <summary>
        /// Detach view from its context
        /// </summary>
        public void Detach()
        {
            if (_context != null)
            {
                _context.DetachView(this);

                OnDetach();
                _context = null;
            }                        
        }

        /// <summary>
        /// Will call every time when context notify it
        /// </summary>
        protected virtual void OnUpdate() { }
        
        /// <summary>
        /// Calls once after binding
        /// </summary>
        protected virtual void OnAttach() { }
        
        protected virtual void OnDetach() { }
    }
}