using System.Collections.Generic;
using System.Threading.Tasks;
using Assets.Game.Domain.Views;
using UnityEngine;
using UnityEngine.AddressableAssets;

namespace Assets.Game.Core.Common
{
    public static class View
    {
        private static readonly Dictionary<string, IContextView> Views = new Dictionary<string, IContextView>();
        
        public static bool TryAdd(IContextView view)
        {
            return Views.TryAdd(view.GetType().Name, view);
        }
        
        public static void Remove(IContextView view)
        {
            var typeName = view.GetType().Name;
            if (Views.ContainsKey(typeName))
            {
                Views.Remove(typeName);
            }
        }

        public static async Task<T> GetOrCreate<T>() where T : MonoBehaviour, IContextView
        {
            var typeName = typeof(T).Name;

            if (Views.TryGetValue(typeName, out var view))
            {
                return (T)view;
            }
            
            var handle = Addressables.LoadAssetAsync<GameObject>(typeName);
            await handle.Task;
            
            view = Object.Instantiate(handle.Result).GetComponent<T>();
            Views[typeName] = view;
            
            Addressables.Release(handle);
            return (T)view;
        }
    }
}