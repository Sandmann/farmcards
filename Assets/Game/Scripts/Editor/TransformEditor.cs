﻿namespace Assets.Source.Scripts.Editor
{
    using UnityEditor;
    using UnityEngine;

    [CustomEditor(typeof(Transform))]
    public class TransformEditor : UnityEditor.Editor
    {
        public static Vector3 resetPosition = Vector3.zero;
        public static Vector3 resetRotation = Vector3.zero;
        public static Vector3 resetScale = Vector3.one;

        SerializedProperty mPos;
        SerializedProperty mRot;
        SerializedProperty mScale;

        private static float _buttonWidth = 120.0f;

        protected void OnEnable()
        {
            mPos = serializedObject.FindProperty("m_LocalPosition");
            mRot = serializedObject.FindProperty("m_LocalRotation");
            mScale = serializedObject.FindProperty("m_LocalScale");
        }

        public override void OnInspectorGUI()
        {
            EditorGUIUtility.labelWidth = 15.0f;

            serializedObject.Update();

            DrawPosition();
            DrawRotation();
            DrawScale();

            serializedObject.ApplyModifiedProperties();
        }

        private void DrawPosition()
        {
            GUILayout.BeginHorizontal();
            bool reset = GUILayout.Button("Position", GUILayout.Width(_buttonWidth));
            EditorGUILayout.PropertyField(mPos.FindPropertyRelative("x"));
            EditorGUILayout.PropertyField(mPos.FindPropertyRelative("y"));
            EditorGUILayout.PropertyField(mPos.FindPropertyRelative("z"));
            GUILayout.EndHorizontal();

            if (reset) mPos.vector3Value = Vector3.zero;
        }

        private void DrawScale()
        {
            GUILayout.BeginHorizontal();
            {
                bool reset = GUILayout.Button("Scale", GUILayout.Width(_buttonWidth));

                EditorGUILayout.PropertyField(mScale.FindPropertyRelative("x"));
                EditorGUILayout.PropertyField(mScale.FindPropertyRelative("y"));
                EditorGUILayout.PropertyField(mScale.FindPropertyRelative("z"));

                if (reset) mScale.vector3Value = Vector3.one;
            }
            GUILayout.EndHorizontal();
        }

        private void DrawRotation()
        {
            GUILayout.BeginHorizontal();
            {
                bool reset = GUILayout.Button("Rotation", GUILayout.Width(_buttonWidth));

                RotationPropertyField();

                if (reset) mRot.quaternionValue = Quaternion.identity;
            }
            GUILayout.EndHorizontal();
        }

        private void RotationPropertyField()
        {
            Transform transform = (Transform)this.targets[0];
            Quaternion localRotation = transform.localRotation;
            foreach (UnityEngine.Object t in (UnityEngine.Object[])this.targets)
            {
                if (!SameRotation(localRotation, ((Transform)t).localRotation))
                {
                    EditorGUI.showMixedValue = true;
                    break;
                }
            }

            EditorGUI.BeginChangeCheck();

            var value = localRotation.eulerAngles;
            value.x = Mathf.Round(value.x);
            value.y = Mathf.Round(value.y);
            value.z = Mathf.Round(value.z);

            Vector3 eulerAngles = EditorGUILayout.Vector3Field("", value);

            if (EditorGUI.EndChangeCheck())
            {
                Undo.RecordObjects(this.targets, "Rotation Changed");
                foreach (UnityEngine.Object obj in this.targets)
                {
                    Transform t = (Transform)obj;
                    t.localEulerAngles = eulerAngles;
                }
                mRot.serializedObject.SetIsDifferentCacheDirty();
            }

            EditorGUI.showMixedValue = false;
        }

        private bool SameRotation(Quaternion rot1, Quaternion rot2)
        {
            if (rot1.x != rot2.x) return false;
            if (rot1.y != rot2.y) return false;
            if (rot1.z != rot2.z) return false;
            if (rot1.w != rot2.w) return false;
            return true;
        }    
    }
}