﻿#if UNITY_EDITOR
namespace Assets.Source.Scripts.Editor
{
    using UnityEditor;
    using UnityEditor.SceneManagement;
    using UnityEngine;
    using UnityEngine.SceneManagement;

    [InitializeOnLoad]
    public class AutosaveOnRun : ScriptableObject
    {
        static AutosaveOnRun()
        {
            EditorApplication.playModeStateChanged += (mode) =>
            {
                if (EditorApplication.isPlayingOrWillChangePlaymode && !EditorApplication.isPlaying)
                {
                    var scene = SceneManager.GetActiveScene();

                    Debug.Log("Auto-Saving scene before entering Play mode: " + scene.name);

                    EditorSceneManager.SaveScene(scene);
                    AssetDatabase.SaveAssets();
                }
            };
        }
    }
}
#endif
